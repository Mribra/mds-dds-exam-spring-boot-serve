# Spring Boot JPA MySQL - Building Rest CRUD API example

## Install
- Faites "java -version". Il faut que ce soit un JDK (et non un JRE). Si cela ne fonctionne pas, installez java : https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.exe
- Installer maven : https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.zip (unzip dans un répertoire de dev), ajouter la variable M2_HOME et ajouter au PATH (cf :https://howtodoinjava.com/maven/how-to-install-maven-on-windows/).
- Tester si maven fonctionne : "mvn --version"
- Installer Mysql 8 si ce n'est pas déjà fait
- lancer "mvn install" dans le répertoire
- Modifier le fichier : src/main/resources/application.properties



## Run Spring Boot application
```
mvnw spring-boot:run 
```

- Tester l'app : http://localhost:8080



docker : 1) docker build -t my-java-app .
         2) docker run -it -p 8080:8080 --env DB_HOST="host" --env  DB_PORT=port --env DB_NAME="name" --env DB_USER="username" --env DB_PASSWORD="pass" my-java-app
          (ne pas oublier de mettre les accolades)

compose :

docker compose -f dockercompose.yml up

front : localhost:1236
back : localhost:9996
NE PAS OUBLIER DE CHANGER LES PARAMETRES DANS LE FICHIER DOCKERCOMPOSE 


Deploy Heroku via dockerfile:

git add .
git commit -m "commit"
heroku stack:set container
git push heroku main
