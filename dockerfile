FROM maven:latest AS maven


COPY src /home/app/src
COPY pom.xml /home/app
# Compile and package the application to an executable JAR
RUN mvn -f /home/app/pom.xml clean -DskipTests package 

# For Java 11, 
FROM openjdk:latest

COPY --from=maven /home/app/target/*.jar app.jar

# Copy the spring-boot-api-tutorial.jar from the maven stage to the /opt/app directory of the current stage.


ENTRYPOINT ["java","-jar","app.jar"]

#Commandes :
#docker build -t my-java-app .
#docker run -p 8080:8080 --env DB_HOST=host --env  DB_PORT=port --env DB_NAME=name --env DB_USER=username --env DB_PASSWORD=pass my-java-app